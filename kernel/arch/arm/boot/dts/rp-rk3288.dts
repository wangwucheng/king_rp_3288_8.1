/*
 * This file is dual-licensed: you can use it either under the terms
 * of the GPL or the X11 license, at your option. Note that this dual
 * licensing only applies to this file, and not this project as a
 * whole.
 *
 *  a) This file is free software; you can redistribute it and/or
 *     modify it under the terms of the GNU General Public License as
 *     published by the Free Software Foundation; either version 2 of the
 *     License, or (at your option) any later version.
 *
 *     This file is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 * Or, alternatively,
 *
 *  b) Permission is hereby granted, free of charge, to any person
 *     obtaining a copy of this software and associated documentation
 *     files (the "Software"), to deal in the Software without
 *     restriction, including without limitation the rights to use,
 *     copy, modify, merge, publish, distribute, sublicense, and/or
 *     sell copies of the Software, and to permit persons to whom the
 *     Software is furnished to do so, subject to the following
 *     conditions:
 *
 *     The above copyright notice and this permission notice shall be
 *     included in all copies or substantial portions of the Software.
 *
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *     EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 *     OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *     NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *     HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 *     WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *     FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 *     OTHER DEALINGS IN THE SOFTWARE.
 */

/dts-v1/;

#include <dt-bindings/pwm/pwm.h>
#include <dt-bindings/input/input.h>
#include "rk3288.dtsi"
#include "rk3288-android.dtsi"
//#include "lcd-rpdzkj-hdmi.dtsi"
//#include "lcd-rpdzkj-hdmi-4k.dtsi"
//#include "lcd-rpdzkj-lvds-10_1_1024_600.dtsi"
#include "lcd-rpdzkj-mipi-10_1_1920_1200.dtsi"
//#include "lcd-rpdzkj-mipi-7_800_1280.dtsi"
//#include "lcd-rpdzkj-mipi-5_720_1280.dtsi"
//#include "lcd-rpdzkj_dual_lvds.dtsi"
//#include "lcd-rpdzkj_dual_lvds_1920x1080_13.3inch.dtsi"
//#include "lcd-edp.dtsi"
//#include "dual-lcd-rpdzkj-lvds-10-1024-mipi-10-1920.dtsi"
//#include "lcd-rpdzkj-mipi-7-1200-1920.dtsi"
//#include "lcd-rpdzkj-mipi-7-1024-600.dtsi"

//#include "lcd-rpdzkj_lvds-1280-800.dtsi"

/ {
	compatible = "rockchip,rp-rk3288", "rockchip,rk3288";

	ext_gmac: external-gmac-clock {
		compatible = "fixed-clock";
		#clock-cells = <0>;
		clock-frequency = <125000000>;
		clock-output-names = "ext_gmac";
	};

	vcc_sys: vsys-regulator {
		compatible = "regulator-fixed";
		regulator-name = "vcc_sys";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		regulator-always-on;
		regulator-boot-on;
	};

	vcc_sd: sdmmc-regulator {
		compatible = "regulator-fixed";
		gpio = <&gpio7 11 GPIO_ACTIVE_LOW>;
		pinctrl-names = "default";
		pinctrl-0 = <&sdmmc_pwr>;
		regulator-name = "vcc_sd";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		startup-delay-us = <100000>;
		vin-supply = <&vcc_io>;
	};
	vcc18_dvp: dvp-regulator {
		compatible = "regulator-fixed";
		regulator-name = "vcc18_dvp";
		gpio = <&gpio0 17 GPIO_ACTIVE_HIGH>;
		pinctrl-names = "default";
		enable-active-high = "true";
		pinctrl-0 = <&dvp_pwr>;
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
		startup-delay-us = <100000>;
		vin-supply = <&vcc_sys>;
	};
	vcc_flash: flash-regulator {
		compatible = "regulator-fixed";
		regulator-name = "vcc_flash";
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
		vin-supply = <&vcc_io>;
	};

	vcc_5v: usb-regulator {
		compatible = "regulator-fixed";
		regulator-name = "vcc_5v";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		regulator-always-on;
		regulator-boot-on;
		vin-supply = <&vcc_sys>;
	};

	vcc_host_5v: usb-host-regulator {
		compatible = "regulator-fixed";
		regulator-name = "vcc_host_5v";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		regulator-always-on;
		vin-supply = <&vcc_5v>;
	};

	vcca_codec_V28: usb-host-regulator {
		compatible = "regulator-fixed";
		regulator-name = "vcca_codec_V28";
		regulator-min-microvolt = <2800000>;
		regulator-max-microvolt = <2800000>;
		regulator-always-on;
		vin-supply = <&vcca_codec>;
	};

	wireless-wlan {
		compatible = "wlan-platdata";
		rockchip,grf = <&grf>;
		//wifi_chip_type = "ap6335";
		wifi_chip_type = "ap6212";
		sdio_vref = <1800>;
		WIFI,host_wake_irq = <&gpio4 30 GPIO_ACTIVE_HIGH>;
		status = "okay";
	};

	sdio_pwrseq: sdio-pwrseq {
		compatible = "mmc-pwrseq-simple";
//		clocks = <&hym8563>;
//		clock-names = "ext_clock";
		pinctrl-names = "default";
		pinctrl-0 = <&wifi_enable_h>;

		/*
		 * On the module itself this is one of these (depending
		 * on the actual card populated):
		 * - SDIO_RESET_L_WL_REG_ON
		 * - PDN (power down when low)
		 */
		reset-gpios = <&gpio4 28 GPIO_ACTIVE_LOW>;
	};
	wireless-bluetooth {
		compatible = "bluetooth-platdata";
//		clocks = <&rk808 1>;
//                clock-names = "ext_clock";
		uart_rts_gpios = <&gpio4 19 GPIO_ACTIVE_LOW>;
		pinctrl-names = "default", "rts_gpio";
		pinctrl-0 = <&uart0_rts>;
		pinctrl-1 = <&uart0_gpios>;
		BT,reset_gpio    = <&gpio4 29 GPIO_ACTIVE_HIGH>;
		BT,wake_gpio     = <&gpio4 26 GPIO_ACTIVE_HIGH>;
		BT,wake_host_irq = <&gpio4 31 GPIO_ACTIVE_HIGH>;
		status = "okay";
	};

};

&emmc {
	bus-width = <8>;
	cap-mmc-highspeed;
	disable-wp;
	non-removable;
	num-slots = <1>;
	pinctrl-names = "default";
	pinctrl-0 = <&emmc_clk &emmc_cmd &emmc_pwr &emmc_bus8>;
	max-frequency = <100000000>;
	mmc-hs200-1_8v;
	mmc-ddr-1_8v;
	status = "okay";
};
/*
&emmc {
        bus-width = <8>;
        cap-mmc-highspeed;
        disable-wp;
        non-removable;
        num-slots = <1>;
        pinctrl-names = "default";
        pinctrl-0 = <&emmc_clk>, <&emmc_cmd>, <&emmc_pwr>, <&emmc_bus8>;
        vmmc-supply = <&vcc_io>;
        vqmmc-supply = <&vcc_flash>;
        status = "okay";
};*/
/*
&cif_isp0 {
	rockchip,camera-modules-attached = <&camera0>;
	status = "disabled";
};*/

&gmac {
	assigned-clocks = <&cru SCLK_MAC>;
	assigned-clock-parents = <&ext_gmac>;
	clock_in_out = "input";
	pinctrl-names = "default";
	pinctrl-0 = <&rgmii_pins>, <&phy_rst>, <&phy_pmeb>, <&phy_int>;
	phy-supply = <&vccio_pmu>;
	phy-mode = "rgmii";
	snps,reset-active-low;
	snps,reset-delays-us = <0 10000 1000000>;
	snps,reset-gpio = <&gpio4 8 GPIO_ACTIVE_LOW>;
	tx_delay = <0x30>;
	rx_delay = <0x10>;
        //status = "disabled";
	status = "okay";
};

&gpu {
	status = "okay";
	mali-supply = <&vdd_gpu>;
};



&hevc_service {
	status = "okay";
};

&i2c0 {
	clock-frequency = <400000>;
	status = "okay";
	vdd_cpu: syr827@40 {
		compatible = "silergy,syr827";
		fcs,suspend-voltage-selector = <1>;
		reg = <0x40>;
		regulator-name = "vdd_cpu";
		regulator-min-microvolt = <850000>;
		regulator-max-microvolt = <1350000>;
		regulator-always-on;
		regulator-boot-on;
		regulator-enable-ramp-delay = <300>;
		regulator-ramp-delay = <8000>;
		vin-supply = <&vcc_sys>;
		regulator-state-mem {
			regulator-off-in-suspend;
		};
	};

	vdd_gpu: syr828@41 {
		compatible = "silergy,syr828";
		fcs,suspend-voltage-selector = <1>;
		reg = <0x41>;
		regulator-name = "vdd_gpu";
		regulator-min-microvolt = <850000>;
		regulator-max-microvolt = <1350000>;
		regulator-always-on;
		regulator-ramp-delay = <6000>;
		vin-supply = <&vcc_sys>;
		regulator-state-mem {
			regulator-off-in-suspend;
		};
	};

	act8846: act8846@5a {
		compatible = "active-semi,act8846";
		reg = <0x5a>;
		status = "okay";
		pinctrl-names = "default";
		//pinctrl-0 = <&pmic_vsel>, <&pmic_sleep>, <&pwr_hold>;
		pinctrl-0 = <&pmic_vsel>, <&pwr_hold>;
		system-power-controller;

		vp1-supply = <&vcc_sys>;
		vp2-supply = <&vcc_sys>;
		vp3-supply = <&vcc_sys>;
		vp4-supply = <&vcc_sys>;
		inl1-supply = <&vcc_io>;
		inl2-supply = <&vcc_sys>;
		inl3-supply = <&vcc_20>;

		regulators {
			vcc_ddr: REG1 {
				regulator-name = "VCC_DDR";
				regulator-min-microvolt = <1200000>;
				regulator-max-microvolt = <1200000>;
				regulator-always-on;
			};

			vcc_io: REG2 {
				regulator-name = "VCC_IO";
				regulator-min-microvolt = <3300000>;
				regulator-max-microvolt = <3300000>;
				regulator-always-on;
			};

			vdd_log: REG3 {
				regulator-name = "VDD_LOG";
				regulator-min-microvolt = <1100000>;
				regulator-max-microvolt = <1100000>;
				regulator-always-on;
			};

			vcc_20: REG4 {
				regulator-name = "VCC_20";
				regulator-min-microvolt = <2000000>;
				regulator-max-microvolt = <2000000>;
				regulator-always-on;
			};

			vccio_sd: REG5 {
				regulator-name = "VCCIO_SD";
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <3300000>;
				regulator-always-on;
			};

			vdd10_lcd: REG6 {
				regulator-name = "VDD10_LCD";
				regulator-min-microvolt = <1000000>;
				regulator-max-microvolt = <1000000>;
				regulator-always-on;
			};

			vcca_codec: REG7 {
				regulator-name = "VCCA_CODEC";
				//regulator-min-microvolt = <1800000>;
				//regulator-max-microvolt = <1800000>;
				regulator-min-microvolt = <2800000>;
				regulator-max-microvolt = <2800000>;
				//regulator-max-microvolt = <3300000>;
			};

			vcca_tp: REG8 {
				regulator-name = "VCCA_TP";
				regulator-min-microvolt = <3300000>;
				regulator-max-microvolt = <3300000>;
				regulator-always-on;
			};

			vccio_pmu: REG9 {
				regulator-name = "VCCIO_PMU";
				regulator-min-microvolt = <3300000>;
				regulator-max-microvolt = <3300000>;
				regulator-always-on;
			};

			vdd_10: REG10 {
				regulator-name = "VDD_10";
				regulator-min-microvolt = <1000000>;
				regulator-max-microvolt = <1000000>;
				regulator-always-on;
			};

			vcc_18: REG11 {
				regulator-name = "VCC_18";
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-always-on;
			};

			vcc18_lcd: REG12 {
				regulator-name = "VCC18_LCD";
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-always-on;
			};
		};
	};
};

&i2c3 {
	status = "okay";
	/*camera0: camera-module@10 {
		status = "disabled";

		compatible = "omnivision,ov8858-v4l2-i2c-subdev";
		reg = <0x10>;
		device_type = "v4l2-i2c-subdev";

		clocks = <&cru SCLK_VIP_OUT>;
		clock-names = "clk_cif_out";

		pinctrl-names = "rockchip,camera_default",
			"rockchip,camera_sleep";
		pinctrl-0 = <&cam0_default_pins>;
		pinctrl-1 = <&cam0_sleep_pins>;

		rockchip,pd-gpio = <&gpio2 15 GPIO_ACTIVE_LOW>;
		rockchip,pwr-gpio = <&gpio0 17 GPIO_ACTIVE_HIGH>;

		rockchip,camera-module-mclk-name = "clk_cif_out";
		rockchip,camera-module-dovdd = "1.8v";
		rockchip,camera-module-facing = "back";
		rockchip,camera-module-name = "cmk-cb0695-fv1";
		rockchip,camera-module-len-name = "lg9569a2";
		rockchip,camera-module-fov-h = "66.0";
		rockchip,camera-module-fov-v = "50.1";
		rockchip,camera-module-orientation = <0>;
		rockchip,camera-module-iq-flip = <0>;
		rockchip,camera-module-iq-mirror = <0>;
		rockchip,camera-module-flip = <0>;
		rockchip,camera-module-mirror = <0>;

		// resolution.w, resolution.h, defrect.left, defrect.top, defrect.w, defrect.h 
		rockchip,camera-module-defrect0 = <3264 2448 0 0 3264 2448>;
		rockchip,camera-module-flash-support = <0>;
		rockchip,camera-module-mipi-dphy-index = <0>;
	};*/
};

&i2c4 {
	status = "okay";
};


&i2c5 {
        status = "disabled";
};


&io_domains {
	status = "okay";

	sdcard-supply = <&vccio_sd>;
	wifi-supply = <&vcc_18>;

        audio-supply = <&vcca_codec>;
//
	bb-supply = <&vcc_io>;
	dvp-supply = <&vcc18_dvp>;
	flash0-supply = <&vcc_flash>;
//        flash1-supply = <&vcc_io>;
        gpio30-supply = <&vccio_pmu>;
        gpio1830-supply = <&vcc_io>;
//        lcdc-supply = <&vcc_io>;
};

&rga {
	status = "okay";
};

&pinctrl {
	pcfg_output_high: pcfg-output-high {
		output-high;
	};

	pcfg_output_low: pcfg-output-low {
		output-low;
	};

	pcfg_pull_none_drv_8ma: pcfg-pull-none-drv-8ma {
		drive-strength = <8>;
	};

	pcfg_pull_up_drv_8ma: pcfg-pull-up-drv-8ma {
		bias-pull-up;
		drive-strength = <8>;
	};
	act8846 {
                pmic_vsel: pmic-vsel {
                        rockchip,pins = <7 10 RK_FUNC_GPIO &pcfg_output_low>;
                };
//		pmic_sleep: pmic-sleep {
//			rockchip,pins = <0 0 RK_FUNC_GPIO &pcfg_output_low>;
//		};
		pwr_hold: pwr-hold {
			rockchip,pins = <0 10 RK_FUNC_GPIO &pcfg_output_high>;
		};
	};

	dvp {
		dvp_pwr: dvp-pwr {
			rockchip,pins = <0 17 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	gmac {
		phy_int: phy-int {
			rockchip,pins = <0 9 RK_FUNC_GPIO &pcfg_pull_up>;
		};

		phy_pmeb: phy-pmeb {
			rockchip,pins = <0 8 RK_FUNC_GPIO &pcfg_pull_up>;
		};

		phy_rst: phy-rst {
			rockchip,pins = <4 8 RK_FUNC_GPIO &pcfg_output_high>;
		};
	};

	sdio-pwrseq {
		wifi_enable_h: wifi-enable-h {
			rockchip,pins = <4 28 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	sdmmc {
		sdmmc_bus4: sdmmc-bus4 {
			rockchip,pins = <6 16 RK_FUNC_1 &pcfg_pull_up_drv_8ma>,
					<6 17 RK_FUNC_1 &pcfg_pull_up_drv_8ma>,
					<6 18 RK_FUNC_1 &pcfg_pull_up_drv_8ma>,
					<6 19 RK_FUNC_1 &pcfg_pull_up_drv_8ma>;
		};

		sdmmc_clk: sdmmc-clk {
			rockchip,pins = <6 20 RK_FUNC_1 &pcfg_pull_none_drv_8ma>;
		};

		sdmmc_cmd: sdmmc-cmd {
			rockchip,pins = <6 21 RK_FUNC_1 &pcfg_pull_up_drv_8ma>;
		};

		sdmmc_pwr: sdmmc-pwr {
			rockchip,pins = <7 11 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};
	/*cam_pins {
		cam0_default_pins: cam0-default-pins {
			rockchip,pins = <0 17 RK_FUNC_GPIO &pcfg_pull_none>,
					<2 15 RK_FUNC_GPIO &pcfg_pull_none>,
					<2 11 RK_FUNC_1 &pcfg_pull_none>;
		};
		cam0_sleep_pins: cam0-sleep-pins {
			rockchip,pins = <0 17 RK_FUNC_GPIO &pcfg_pull_none>,
					<2 15 RK_FUNC_GPIO &pcfg_pull_none>,
					<2 11 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};*/

	otg_vbus_drv {
		otg_vbus_drv: otg-vbus-drv {
			rockchip,pins = <0 12 RK_FUNC_GPIO &pcfg_output_high>;
		};
	};

	wireless-bluetooth {
		uart0_gpios: uart0-gpios {
			rockchip,pins = <4 19 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};
};

&isp {
	status = "okay";
};

&isp_mmu {
	status = "okay";
};


&sdio0 {
	status = "okay";
	max-frequency = <150000000>;

	bus-width = <4>;
	cap-sd-highspeed;
	cap-sdio-irq;
	disable-wp;
	keep-power-in-suspend;
	mmc-pwrseq = <&sdio_pwrseq>;
	non-removable;
	num-slots = <1>;
	pinctrl-names = "default";
	pinctrl-0 = <&sdio0_bus4 &sdio0_cmd &sdio0_clk &sdio0_int>;
	sd-uhs-sdr104;
	supports-sdio;
};

&sdmmc {
	bus-width = <4>;
	cap-mmc-highspeed;
	cap-sd-highspeed;
	card-detect-delay = <200>;
	disable-wp;
	num-slots = <1>;
	pinctrl-names = "default";
	pinctrl-0 = <&sdmmc_clk>, <&sdmmc_cmd>, <&sdmmc_cd>, <&sdmmc_bus4>;
	supports-sd;
	vmmc-supply = <&vcc_sd>;
	vqmmc-supply = <&vccio_sd>;
	status = "okay";
};



&usbphy {
	status = "okay";
};

&usb_host0_ohci {
	status = "okay";
};

&usb_host1 {
	status = "okay";
};

&usb_otg {
	pinctrl-0 = <&otg_vbus_drv>;
	status = "okay";
};
&usb_host0_ehci {
	rockchip-relinquish-port;
	status = "okay";
};
&vpu_service {
	status = "okay";
};

&rockchip_suspend {
	status = "okay";
};
